from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from rest_framework import routers
from AnimalCrossingGuide.api import views

router = routers.DefaultRouter()
router.register(r'animals', views.AnimalViewSet, basename="Animal")
router.register(r'locations', views.LocationViewSet, basename="Location")
router.register(r'categories', views.CategoryViewSet, basename="Category")
router.register(r'shadowsizes', views.ShadowSizeViewSet, basename="ShadowSize")

urlpatterns = [
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


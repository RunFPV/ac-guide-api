from django.db import models
from django.contrib.postgres.fields import ArrayField


class Location(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class ShadowSize(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


def nameFile(instance, filename):
    return '/'.join(['images', str(instance.category.name), filename])


class Animal(models.Model):
    name = models.CharField(max_length=30)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    shadowSize = models.ForeignKey(ShadowSize, on_delete=models.CASCADE, blank=True, null=True)
    rarity = models.IntegerField()
    startHour = models.IntegerField()
    endHour = models.IntegerField()
    months = ArrayField(models.IntegerField())
    price = models.IntegerField()
    img = models.ImageField(upload_to=nameFile, blank=True, null=True)

    def __str__(self):
        return self.name

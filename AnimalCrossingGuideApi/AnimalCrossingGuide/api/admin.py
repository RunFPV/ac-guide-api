from django.contrib import admin
from .models import Animal, Location, Category, ShadowSize


class AnimalAdmin(admin.ModelAdmin):
    list_display = ('name', 'rarity', 'shadowSize', 'location', 'price', 'img')
    list_filter = ['category']


admin.site.register(Animal, AnimalAdmin)
admin.site.register(Location)
admin.site.register(Category)
admin.site.register(ShadowSize)
